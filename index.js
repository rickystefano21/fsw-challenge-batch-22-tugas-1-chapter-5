const {v4: uuidv4} = require('uuid');
const fs = require("fs");

class FileManager{
constructor(filename){
this.filename = filename
this.data = this.#readFile(filename)
}

#readFile(path){
  const datatxt = fs.readFileSync(path, {encoding: "utf-8"})
  return JSON.parse(datatxt)
}

addData(newData,path){
  let finaldata = this.data
  finaldata.push(newData)
  const _datastr = JSON.stringify(finaldata)
  fs.writeFileSync(this.filename, _datastr)
  return true
}

deleteData(id){
  let finalData = this.data
  let idx = finalData.findIndex(x => x.id ===id);
  finalData.splice(idx,1)
  fs.writeFileSync(this.filename, JSON.stringify(finalData))
}

editdata(id,name){
  let finalData = this.data
  let idx = finalData.findIndex((x => x.id === id));
  finalData[idx].name = name;
  fs.writeFileSync(this.filename, JSON.stringify(finalData))
}
}

const filename = "data.txt"
const file = new FileManager(filename)
// const newData = {
// id : uuidv4(),
// name : "Ming",
// address : "earth"por
// }
file.editdata("0ef7188d-32dc-41d2-b6ac-9409682490d8","Lucy")
